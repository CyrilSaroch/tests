def mySplit(word : str):
    if len(word) == 1:
        return [word]

    result = []
    result.append("")
    indexOfResult = 0
    lastChar = word[0]
    repeatedChar = 0x00
    length = len(word)

    for i in range(1, length):
        char = word[i]
        
        if char == lastChar or lastChar == repeatedChar:
            result[indexOfResult] += "*"
            repeatedChar = lastChar
        else:
            if not lastChar == ' ':
                # This happens when we've just created a new element of the array (because of a space)
                result[indexOfResult] += lastChar
            repeatedChar = 0x00

        if char == ' ':
            indexOfResult += 1
            result.append("")
        
        lastChar = char

    if word[length - 1] == repeatedChar:
        result[indexOfResult] += "*"
    else:
        result[indexOfResult] += word[length - 1]

    return result

def reverse_string(words : str):
    split = mySplit(words) # Split string by spaces, and replace repeated characters.
    reversed = "";
    for i in range(0, len(split) - 1):
        reversed += split[len(split) - i - 1]
        reversed += " "
    
    reversed += split[0] # This is so that there isn't a space at the end of the string
    return reversed
        

def reverse_words(arguments : [str]):
    result = []
    for i in arguments:
        result.append(reverse_string(i))
    
    return result