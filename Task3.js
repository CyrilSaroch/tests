// NOTE:
// The given methods for determining divisibility do not specify how to check the final sum.
// I chose to simply see the remainder after division, but since the whole problem could have
// been solved by this, I also provide an example of divisible3 called 'divisible3_2', which
// shows how the '%' operator does not need to be used, just in case I wasn't meant to use '%'
// at all. divisible11_2 would be analogous.

function divisible3_2(number) {
    if ((Math.abs(number) < 10)) {
        if (number == 0 || number == 3 || number == 6 || number == 9) {
            return true;
        } else {
            return false;
        }
    }

    let sum = 0;
    while (number != 0) {
        sum += number - Math.floor(number / 10) * 10;
        number = Math.floor(number / 10);
    }
    return divisible3_2(sum);
}

function divisible3(number) {
    let sum = 0;
    while (number != 0) {
        sum += number % 10;
        number = Math.floor(number / 10);
    }
    return sum % 3 == 0;
}

function divisible11(number) {
    let sum = 0;
    while (number != 0) {
        sum += number % 100;
        number = Math.floor(number / 100);
    }
    return sum % 11 == 0;
}

function divisible(number) {
    let word = '';
    if (divisible3(number)) {
        word += 'fizz';
    }
    if (divisible11(number)) {
        word += 'buzz';
    }
    if (word == '') {
        word = 'baz';
    }
    return word;
}

function task3(numbers) {
    let transformed = numbers.map(divisible);
    return transformed;
}