public class Task1 {
    public static int nthElement(String arguments) {
        String[] args = arguments.split(" "); // Separate individual arguments
        int a1 = Integer.parseInt(args[0]);
        int a2 = Integer.parseInt(args[1]);
        int n = Integer.parseInt(args[2]);

        // Count up to n
        for (int i = 3; i <= n; i++) {
            int temp = a2 + a1;
            a1 = a2;
            a2 = temp;
        }

        // I wasn't told what "outup" means in this case. I only return, but a
        // System.out.println(a2);
        // would also be a possibility.
        return a2;
    }
}